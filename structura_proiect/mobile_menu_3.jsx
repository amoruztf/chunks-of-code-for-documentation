'use strict';
var T = 'HeaderMobile',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	string = require('visual/model/basic/string'),
	MenuMobile = require('visual/model/complex/MenuMobile'),
	COMMON_SCRIPTS = require('../../../assets/js/common.js');

ModelDefinition[T] = MenuMobile.ModelDefinition.extend({
	properties: {
		menuId: string.property('header-menu')
	}
});

VisualDefinition[T] = MenuMobile.VisualDefinition.extend({
	initOptions: function () {
		return COMMON_SCRIPTS.MobileMenu.getOptionsSimple();
	},
	wrapperMenu: function (item_list) {}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
