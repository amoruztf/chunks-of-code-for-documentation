'use strict';

var T = 'IconColor',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object');

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {}
});

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
