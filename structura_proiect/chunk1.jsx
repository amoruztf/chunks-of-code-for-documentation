'use strict';
var T = 'BlockName',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition');

ModelDefinition[T] = require('visual/model/basic/object').ModelDefinition.extend({
	properties: {},
	visual: {
		title: T,
		screenshot: require('./preview.jpg')
	}
});

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function (v, Vi) {
		return <div>Demo content.</div>;
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
