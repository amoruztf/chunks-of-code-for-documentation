...

VisualDefinition[T] = VisualDefinition.extend({
	defaultProps: {
		className: 'btn'
	},
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="link">
				<Vi property="text">
					<span className={this.props.className}>
						<span>{v.text}</span>
					</span>
				</Vi>
			</Vi>
		);
	}
});

...
