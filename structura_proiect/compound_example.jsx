...

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {
		icon: icon.property('fa fa-facebook'),
		color: color.property('#000'),
		link: link.property('http://example.com')
	}
});

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="color" compound={[v.icon, v.link]}>
				<Vi property="icon" compound={[v.link]}>
					<Vi property="link">
					    <span>Content here...</span>
					</Vi>
				</Vi>
			</Vi>
		);

	}
});

// Inca un exemplu:

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="color" compound={[v.icon, v.link]}>
				<Vi property="icon">Content here...</Vi>
				<Vi property="link">Content here...</Vi>
			</Vi>
		);
	}
});

...
