...

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function (v, Vi) {
		return (
			<section className="articalMedia">
				<Vi property="background" compound={[v.button]}>
					<div className="container section-padding-1">
						<div className="row">
							<div className="col-md-8 col-md-offset-2 artical-top-desc paragrap-2 headings-2">
								<BarNoExtend>
									<Vi property="button" className="btn btn-default" />
								</BarNoExtend>
							</div>
						</div>
					</div>
				</Vi>
			</section>
		)
	}
});
