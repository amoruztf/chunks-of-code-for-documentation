'use strict';

var T = 'Header',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	string = require('visual/model/basic/string'),
	MenuContainer = require('visual/model/complex/Menu/MenuContainer'),
	MobileMenu = require('./MobileMenu');

ModelDefinition[T] = MenuContainer.ModelDefinition.extend({
	properties: {
		menuId: string.property('header-menu'),
		mobile_menu: MobileMenu.property()
	},
	visual: {
		title: 'Header',
		screenshot: require('./Preview.jpg')
	}
});

VisualDefinition[T] = MenuContainer.VisualDefinition.extend({
	renderForEdit: function (v, Vi) {
		return (
			<header className="header">
				<Vi property="mobile_menu" />
			</header>
		);
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
