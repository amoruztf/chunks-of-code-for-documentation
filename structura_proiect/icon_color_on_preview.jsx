VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
		var style = {
			color: v.color
		};
		return (
			<Vi property="color" onPreview={this.handlePreviewColor} compound={[v.icon]}>
				<Vi property="icon">
					<i className={v.icon} style={style}></i>
				</Vi>
			</Vi>
		);
	},
	handlePreviewColor: function(value) {
		jQuery(this.getDOMNode()).css('color', value);
	}
});
