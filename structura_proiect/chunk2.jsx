'use strict';

var T = 'feature.item',
    React = require('react'),
    ModelDefinition = require('visual/ModelDefinition'),
    VisualDefinition = require('visual/VisualDefinition'),
    BarNoExtend = require('visual/component/bar/BarNoExtend');

ModelDefinition[T] = require('visual/model/basic/object').ModelDefinition.extend({
    properties: {}
});

VisualDefinition[T] = VisualDefinition.extend({
    renderForEdit: function(v, Vi) {
        return (
            <div>Content here...</div>
        );
    }
});

module.exports = {
    ModelDefinition: ModelDefinition[T],
    VisualDefinition: VisualDefinition[T],
    type: T,
    property: function (value) {
        return {type: T, value: value};
    }
};
