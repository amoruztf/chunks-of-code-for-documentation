...

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="link">
				<Vi property="text">
					<span className={this.props.className}>
						<span>{v.text}</span>
					</span>
				</Vi>
			</Vi>
		);
	}
});

...
