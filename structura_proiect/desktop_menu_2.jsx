'use strict';

var T = 'Header',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	string = require('visual/model/basic/string'),
	BarNoExtend = require('visual/component/bar/BarNoExtend'),
	Menu = require('./Menu');

ModelDefinition[T] = MenuContainer.ModelDefinition.extend({
	properties: {
		menuId: string.property('header-menu'),
		menu: Menu.property(),

	},
	visual: {
		title: 'Header',
		screenshot: require('./Preview.jpg')
	}
});

VisualDefinition[T] = MenuContainer.VisualDefinition.extend({
	renderForEdit: function (v, Vi) {
		var logoLink = this.getLogoLink();
		return (
			<header className="header">
				<div className="header-menu menu">
					<Vi property="menu" />
				</div>
			</header>
		);
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
