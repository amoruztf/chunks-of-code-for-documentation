'use strict';

var T = 'BlockName',
    React = require('react'),
    ModelDefinition = require('visual/ModelDefinition'),
    VisualDefinition = require('visual/VisualDefinition'),
    object = require('visual/model/basic/object'),
    Background = require('../../basic/Background'),
    Medium = require('visual/3rd-party/Medium'),
    Features = require('./features')
    FeatureItem = require('./feature.item');

ModelDefinition[T] = object.ModelDefinition.extend({
    properties: {
        background: Background.property({
            image: {
                sources:{
                    original: ''
                }
            },
            color: '#fff',
            opacity: '40'
        }),
        topDescription: Medium.property('<h2>LATEST WORKS</h2><p><br /></p><p><br /></p><p>Curabitur eu adipiscing lacus, a iaculis diam. Nullam placerat blandit auctor. Nulla accumsan ipsum et nibh rhoncus, eget tempus sapien ultricies. Donec mollis lorem vehicula.</p>'),
        features: Features.property([
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-flag'
                },
                content: '<h3>WE’RE CREATIVE</h3><p>Lorem ipsum dolor sit amet, c-r adipiscing elit. In maximus ligula semper metus pellentesque mattis. Maecenas volutpat, diam enim.</p>'
            }),
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-clock'
                },
                content: '<h3>WE’RE PUNCTUAL</h3><p>Proin fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend, lorem fermentum orci sit amet, iaculis libero.</p>'
            }),
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-hotairballoon'
                },
                content: '<h3>WE HAVE MAGIC</h3><p>Curabitur iaculis accumsan augue, nec finibus mauris pretium eu. Duis placerat ex gravida nibh tristique porta. Nulla facilisi. Suspendisse ultricies eros blandit.</p>'
            }),
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-heart'
                },
                content: '<h3>WE LOVE MINIMALISM</h3><p>Cras luctus interdum sodales. Quisque quis odio dui. Sedes sit amet neque malesuada, lobortis commodo magna ntesque pharetra metus vivera sagittis.</p>'
            }),
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-linegraph'
                },
                content: '<h3>WE’RE RESPONSIBLE</h3><p>Fusce aliquet quam eget neque ultrices elementum. Nulla posuere felis id arcu blandit sagittis. Eleifender vestibulum purus, sit amet vulputate risus.</p>'
            }),
            FeatureItem.property({
                iconColor: {
                    color: '#000',
                    icon: 'icon-chat'
                },
                content: '<h3>WE\'RE FRIENDLY</h3><p>Pulvinar vitae neque et porttitor. Integer non dapibus diam, ac eleifend lectus. Praesent sed nisi eleifend, fermentum orci sit amet, iaculis libero interdum.</p>'
            }),
        ])
    },
    visual: {
        title: T,
        screenshot: require('./preview.jpg')
    }
});

VisualDefinition[T] = VisualDefinition.extend({
    renderForEdit: function (v, Vi) {
        return (
            <section className="features section-padding-1">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 col-md-offset-2 features-top-desc paragrap-2 headings-2">
                            <Vi property="topDescription" />
                        </div>
                    </div>
                    <Vi property="features" />
                </div>
            </section>
        );
    }
});

module.exports = {
    ModelDefinition: ModelDefinition[T],
    VisualDefinition: VisualDefinition[T],
    type: T,
    property: function (value) {
        return {type: T, value: value};
    }
};
