'use strict';

var T = 'Icon',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object'),
	icon = require('visual/model/generic/icon');

...
