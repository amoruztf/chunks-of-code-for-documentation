...

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="color" compound={[v.icon]}>
				<Vi property="icon">
					<i className={v.icon}></i>
				</Vi>
			</Vi>
		);
	}
});

...
