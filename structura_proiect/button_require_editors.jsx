'use strict';

var T = 'Button',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object'),
	text = require('visual/model/generic/Text'),
	link = require('visual/model/generic/Link');

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {}
});

...
