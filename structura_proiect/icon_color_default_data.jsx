...

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {
		icon: icon.property('fa fa-facebook'),
		color: color.property('#000')
	}
});

...
