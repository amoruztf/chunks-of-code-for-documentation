'use strict';

var T = 'Header.Menu',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	VisualString = require('visual/model/basic/string'),
	Menu = require('visual/model/complex/Menu'),
	MenuLink = require('visual/model/complex/Menu/MenuLink');

ModelDefinition[T] = Menu.ModelDefinition.extend({
	properties: {
		menuId: VisualString.property('header-menu')
	}
});

VisualDefinition[T] = Menu.VisualDefinition.extend({
	componentDidMount: function (item) {
		jQuery(this.getDOMNode()).find('.nav').navOffset();
	},
	componentDidUpdate: function () {
		jQuery(this.getDOMNode()).find('.nav').navOffset('refresh');
	},
	renderContainer: function(children, depth) {
		var className = depth === 0 ? 'nav navbar-nav' : 'dropdown-menu';
		return (
			<ul className={className}>
				{children}
			</ul>
		);
	},
	renderItem: function(item, depth) {
		var hasChildren = !!item.children.length,
			className = '',
			caret;

		if (depth === 0) {
			className = 'dropdown';
		} else if (hasChildren) {
			className = 'dropdown-submenu';
			caret = <i className="fa fa-angle-right"></i>;
		}

		var children = item.children.length ? this.renderTree(item, depth + 1) : null;
		className += this.isActivePage(item) ? ' active' : '';

		return (
			<li className={className}>
				<MenuLink item={item}>
					{item.title}
					{caret}
				</MenuLink>
				{children}
			</li>
		);
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
