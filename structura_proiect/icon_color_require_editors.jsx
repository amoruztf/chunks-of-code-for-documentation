'use strict';

var T = 'IconColor',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object'),
	icon = require('visual/model/generic/icon'),
	color = require('visual/model/generic/color');

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {}
});

...
