'use strict';

var T = 'BlankObject',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object'),
	icon = require('visual/model/basicEditors/icon'),
	color = require('visual/model/basicEditors/color');

...
