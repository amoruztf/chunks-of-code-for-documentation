...

VisualDefinition[T] = VisualDefinition.extend({
	defaultProps: {
		className: 'btn'
	},
	handleClickToButton: function(event) {
		jQuery(this.getDOMNode()).find('span').attr('contenteditable', true).focus();
	},
	renderForEdit: function(v, Vi) {
		return (
			<Vi property="link">
				<Vi property="text">
					<span className={this.props.className} onClick={this.handleClickToButton}>
						<span>{v.text}</span>
					</span>
				</Vi>
			</Vi>
		);
	}
});

...
