...

ModelDefinition[T] = object.ModelDefinition.extend({
	properties: {
		text: text.property('Some text'),
		link: link.property()
	}
});

...
