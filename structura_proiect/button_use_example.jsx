'use strict';
var T = 'DemoBlock',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	Background = require('../../basic/Background'),
	button = require('../../basic/Button'),
	BarNoExtend = require('visual/component/bar/BarNoExtend');

ModelDefinition[T] = require('visual/model/basic/object').ModelDefinition.extend({
	properties: {
		background: Background.property({
			image: {
				original: ''
			},
			color: '#ffffff',
			opacity: '40'
		}),
		button: button.property({
			text: 'Some text',
			link: 'http://themefuse.com'
		})
	},
	visual: {
		title: 'Demo block',
		screenshot: require('./preview.png')
	}
});

VisualDefinition[T] = VisualDefinition.extend({
	renderForEdit: function (v, Vi) {
		return (
			<section className="articalMedia">
				<Vi property="background" compound={[v.button]}>
					<div className="container section-padding-1">
						<div className="row">
							<div className="col-md-8 col-md-offset-2 artical-top-desc paragrap-2 headings-2">
								<BarNoExtend>
									<Vi property="button" className="btn btn-default" />
								</BarNoExtend>
							</div>
						</div>
					</div>
				</Vi>
			</section>
		)
	}
});

...
