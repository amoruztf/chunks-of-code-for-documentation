<BlockOver  overClassName="team-wrap-over">
	<Vi property="image"
		compound={[v.content, v.iconLink, v.description]}
		imageSize="360x438"
		sourceSize="360x438"
		onPreview={this.handlePreviewImage}>
			<div className="team-item">
				<Vi property="image" ref="image_preview" withOutSize={true}/>
				<div className="team-overlay-color"></div>
				...
			</div>
	</Vi>
</BlockOver>
