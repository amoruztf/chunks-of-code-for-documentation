'use strict';

var T = 'features_with_icon.array',
    React = require('react'),
    ModelDefinition = require('visual/ModelDefinition'),
    VisualDefinition = require('visual/VisualDefinition');

ModelDefinition[T] = require('visual/model/basic/arrayByBlock').ModelDefinition.extend({
    items: [],
    value: []
});

VisualDefinition[T] = require('visual/model/basic/arrayByBlock').VisualDefinition.extend({
    wrapContainer: function (children) {
        return <div className="row row-grid">{children}</div>;
    },
    wrapItem: function (item, itemIndex) {
        return <div className="col-sm-6 col-md-4">{item}</div>;
    }
});

module.exports = {
    ModelDefinition: ModelDefinition[T],
    VisualDefinition: VisualDefinition[T],
    type: T,
    property: function (value) {
        return {type: T, value: value};
    }
};
