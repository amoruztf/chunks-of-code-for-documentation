...

VisualDefinition[T] = VisualDefinition.extend({
	getMarkup: function(v, Vi) {
		return (
			<Vi property="icon">
				<i className={v.icon}></i>
			</Vi>
		);
	}
});

...
