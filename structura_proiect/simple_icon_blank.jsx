'use strict';

var T = 'BlankObject',
	React = require('react'),
	ModelDefinition = require('visual/ModelDefinition'),
	VisualDefinition = require('visual/VisualDefinition'),
	object = require('visual/model/basic/object');

ModelDefinition[T] = object.ModelDefinition.extend({
	properies: {}
});

VisualDefinition[T] = VisualDefinition.extend({
	getMarkup: function(v, Vi) {
		return <span>Content here...</span>;
	}
});

module.exports = {
	ModelDefinition: ModelDefinition[T],
	VisualDefinition: VisualDefinition[T],
	type: T,
	property: function (value) {
		return {type: T, value: value};
	}
};
