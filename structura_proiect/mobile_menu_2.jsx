COMMON_SCRIPTS.MobileMenu = {
	defaultOptions: function(options) {
		return extend({
			className: "mmenu-jquery",
			openingInterval: 25,
			transitionDuration: 400,
			autoHeight: false,
			onClick: {
				preventDefault: false,
				close: true
			},
			navbar: {
				title: false
			},
			offCanvas: {}
		}, options);
	},
	getOptionsSimple: function() {
		return this.defaultOptions({
			offCanvas: {
				position: 'top',
				zposition: 'next'
			},
			autoHeight: true
		});
	}
}
